# Shell-Tips-and-Tricks

Collection of day-to-day, simple & handy tricks and short-hands that are used for ease of operating Linux. Approach is constructive. Not generic ones, but personal trails.

## Table of Contents
1. [Functions](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/tree/master/functions)
	1. [lsdir-lsfile](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/functions/lsdir-lsfile.sh)
	2. [clipboard_ops](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/functions/clipboard_ops.sh)
2. [Makefiles](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/tree/master/makefiles)
	1. [git-push](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/makefiles/git-push)
	2. [git-merge](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/makefiles/git-merge)
3. [Tricks](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/tree/master/tricks)
	1. [run-cron-jobs-ad-hoc](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/tricks/run-cron-jobs-ad-hoc.sh)
	2. [type-of-shell](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/tricks/type-of-shell.sh)
---

## _More to be added time-to-time ..._
