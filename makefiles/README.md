# Shell-Tips-and-Tricks -> Makefiles
### 1. [git-push](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/makefiles/git-push)
GNU make rule for add/rm/commit + git push to remote repo.
##### Rules:
1. Not first push. The username, remote repo and mail should be configured prior.
2. The branch name is taken dynamically. The default remote name is 'origin'. You may change below.
3. The default comment can be changed below. Run time comments are allowed.

##### Syntax:
```
# 1. To push with default comment.
make -f git-push git_push
# 2. To push with custom comment. Note: The comment here doesn't have - or -- before it.
make -f git-push comment="Comment Here" git_push
# 3. Or if you rename this file as GNUmakefile (or) makefile (or) Makefile,
#    and place in root of the project, you can use direct convention.
make git_push
make comment="Comment Here" git_push
```

### 2. [git-merge](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/makefiles/git-merge)
GNU make rule for merging commits of current branch to specified branch.
##### Rules:
1. Add, Remove and Commits should be done of current working branch before merging.
2. Merge-to branch should be provided in the command run.
3. Merge-to branch shouldn't be same as current working branch.
4. Merge-to branch should be valid and existing branch.

##### Syntax:
```
# 1. To merge with branch 'master' where current working branch isn't the same.
make -f git-merge git_merge to=master
# 2. Or if you rename this file as GNUmakefile (or) makefile (or) Makefile,
#    and place in root of the project, you can use direct convention.
make git_merge to=master

```
---
## _More to be added here ..._
