#!/bin/bash

# Command to run all the jobs in the crontab to run manually without waiting for schedules.
# Tested only with bash
# Runs from user's crontab.
alias run-cron="crontab -l | awk '/^[^#]/{print substr(\$0, index(\$0,\$6))}' | /bin/bash"
