# Shell-Tips-and-Tricks -> Tricks
### 1. [run-cron-jobs-ad-hoc](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/tricks/run-cron-jobs-ad-hoc.sh)
Shell command for manually run the cron jobs without waiting for schedule.
##### Rules:
1. Works with BASH.
2. Runs jobs from user crontab.

##### Syntax:
```
# Source for the first time
source ./run-cron-jobs-ad-hoc.sh

# Routine use
run-cron
```

### 2. [type-of-shell](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/tricks/type-of-shell.sh)
Shell command to check the type of shell (bash, sh, ksh, etc.)

##### Syntax:
```
# Source for the first time
source ./type-of-shell.sh

# Routine use
shell-type
```
---
## _More to be added here ..._
