# Command to check the type of shell (bash, sh, ksh, etc.)
alias shell-type="echo \$\$ | xargs -I % cat /proc/%/comm"
