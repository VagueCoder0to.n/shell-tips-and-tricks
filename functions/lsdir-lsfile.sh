#! /bin/bash

# List Only Directories - Shortcut
# Syntax:
#       In current dir:
#               lsdir
#       In specific dir
#               lsdir path/to/dir
lsdir()
{
        if [[ -z "$1" ]]; then
                ls -al | sed '1,3d' | awk '/^d/{print}';
        else
                TEMP=${PWD};
                cd $1;
                ls -al | sed '1,3d' | awk '/^d/{print}';
                cd ${TEMP};
        fi

}

# List Only Files - Shortcut
# Syntax:
#       In current dir:
#               lsfile
#       In specific dir
#               lsfile path/to/dir
lsfile()
{
        if [[ -z "$1" ]]; then
                ls -al | sed '1,3d' | awk '/^[^d]/{print}';
        else
                TEMP=${PWD};
                cd $1;
                ls -al | sed '1,3d' | awk '/^[^d]/{print}';
                cd ${TEMP};
        fi

}
