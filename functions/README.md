# Shell-Tips-and-Tricks -> Functions
### 1. [lsdir-lsfile](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/functions/lsdir-lsfile.sh)
BASH functions used for long listing the directories alone and files alone respectively.
##### Syntax:
```
# Directories listing in current dir:
lsdir
# Directories listing in specific dir:
lsdir path/to/dir

# Files listing in current dir:
lsfile
# Files listing in specific dir:
lsfile path/to/dir
```

### 2. [clipboard_ops](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/functions/clipboard_ops.sh)
A local file to use as clipboard. And to add shorthands for clipboard file to read, add and open in editor.

##### Rules:
1. Location of a file should be mentioned in [clipboard_ops](https://gitlab.com/VagueCoder0to.n/shell-tips-and-tricks/-/blob/master/functions/clipboard_ops.sh)'s CLIPBOARD.
2. The user should have write permissions to file. Recomended, file in any sub directory of home.

##### Syntax:
```
# Reading clipboard
u_clip
# Opening clipboard in editor
u_editclip
# Add a word/line to clipboard
u_addclip <word/line>
# Clearing Clipboard
u_clearclipboard
```

##### Pros:
1. If no args are provided, the functions terminates.
2. While adding clip, if the entire word/line exists in clipboard, or any word of entire line matches with any line in clipboard, the function prompts whether to add or not.

---
## _More to be added here ..._
