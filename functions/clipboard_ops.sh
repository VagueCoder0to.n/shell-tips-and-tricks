#! /bin/bash

# Add path to clipboard file below.
# Note: The user should have write permissions to file. Recomended, file in any sub directory of user (~/).
# Note: Give absolute path, not relative to ~/.
CLIPBOARD="<Absolute path to clipboard>"

alias u_clip='cat $CLIPBOARD'
alias u_editclip='nano $CLIPBOARD'

u_addclip()
{
	if [ $# -gt 0 ]; then
		args=$@;
		flag="no";
		if grep -q "$args" ${CLIPBOARD}; then
			grep -x "$args" ${CLIPBOARD}
			flag="yes";
		else
			for arg in $@
			do
				if ! [[ ${arg} =~ ^-.* ]]; then 
					if grep -q ${arg} ${CLIPBOARD}; then
						grep ${arg} ${CLIPBOARD};
						flag="yes";
					fi
				fi
			done
		fi

		if [ $flag == "yes" ]; then
			read -p "Found these matches in clipboard. Still want to write? [y/n] " -n1 status;
			if [ $status == "y" ]; then
				echo >>${CLIPBOARD};
				echo "> "$@>>${CLIPBOARD};
				echo $'\n'"Added to clipboard: $@";
			elif [ $status == "n" ]; then
				echo $'\n'"\"No\" is selected. Exiting.";
			else
				echo $'\n'"Invalid input. Exiting.";
			fi
		else
			echo >>${CLIPBOARD};
			echo "> "$@>>${CLIPBOARD};
			echo "Following added to clipboard: $@";
		fi
	else
		echo "No Arguments. Exiting.";
	fi
}

u_clearclipboard()
{
	printf "# List of clips:\n" >${CLIPBOARD};
	echo "Cleared all clips from clipboard ${CLIPBOARD}";
}
